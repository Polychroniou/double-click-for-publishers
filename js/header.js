(function ($, window, Drupal, drupalSettings) {
    'use strict';
    googletag.cmd.push(function() {
        googletag.defineSlot('/'+drupalSettings.network_code+'/'+drupalSettings.targeted_add_unit, [parseInt(drupalSettings.width), parseInt(drupalSettings.height)], drupalSettings.slot_matching_string).addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
    });
})(jQuery, window, Drupal, drupalSettings);
