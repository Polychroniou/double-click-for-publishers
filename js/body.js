(function ($, window, Drupal, drupalSettings) {
    'use strict';
    googletag.cmd.push(function() { googletag.display(drupalSettings.slot_matching_string); });
})(jQuery, window, Drupal, drupalSettings);
